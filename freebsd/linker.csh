#!/bin/csh

foreach x ( `ls -AI links/` )
    if ( -l links/$x ) then
        set link=`readlink links/$x`
        if ( `stat -f %m $link` > `stat -f %m links/$x` || -e origns/$x == 0 ) then
            ln -sf $link links/
            cp -Rf $link origns/
        endif
    endif
end

