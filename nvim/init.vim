" base config
set nocompatible

let mapleader = ","
" disable F1
nmap <F1> <nop>

" tmux color problem
set t_Co=256
set background=dark

set nobackup
set nowritebackup
set noswapfile
" change buffer without save
"set hidden
set undofile
" browse oldfiles limit
set viminfo='40
"set autochdir

vnoremap < <gv " continue visual selecting after shiftwidh
vnoremap > >gv

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

cmap w!! w !sudo tee > /dev/null %
noremap <silent><Leader>/ :nohls<CR>

"set ignorecase smartcase
set clipboard=unnamedplus
set hlsearch
"set number

" backspace behave as expect
set backspace=indent,eol,start

" nasty file find
set path=$PWD/**
set wildmenu
set wildignore=*.pyc

" template pasting
"nnoremap ,html :-1read ~/.vim/templates/html<CR>

" always show statusline
set laststatus=2
set statusline=%f\ -\ %y

" show trailing whitespaces
au BufNewFile,BufRead * let b:mtrailingws=matchadd('ErrorMsg', '\s\+$', -1)

call plug#begin('~/.config/nvim/plugged')

Plug 'junegunn/fzf.vim'
Plug 'w0rp/ale'
Plug 'Valloric/YouCompleteMe', {'do': './install.py --go-completer'}
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

call plug#end()

" 'syntax' and 'filetype plugin indent' need to be overrided only after call plug#begin()
syntax off
filetype plugin on

" ripgrep
command! -bang -nargs=* Find call fzf#vim#grep('rg -tpy -tjs --column --line-number --no-heading --fixed-strings --ignore-case --follow --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)

" w0rp/ALE
let g:ale_linters = {
\   'python': ['pylint'],
\}
let g:ale_python_pylint_options = '--rcfile $HOME/.pylintrc_google --load-plugins=pylint_django'
nmap <silent> <M-k> <Plug>(ale_previous_wrap)
nmap <silent> <M-j> <Plug>(ale_next_wrap)
" END w0rp/ALE

" YCM
"let g:ycm_python_binary_path = system("pipenv --py")[:-2]
let g:ycm_autoclose_preview_window_after_completion = 1

" no preview window on top of a file
set completeopt-=preview
set completeopt-=menuon
let g:ycm_add_preview_to_completeopt = 0

nnoremap ,d :YcmCompleter GoToDefinitionElseDeclaration<CR>
" END YCM

" start from prev place
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" yaml
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

command! Oldfiles call fzf#run({
\  'source':  v:oldfiles,
\  'sink':    'e',
\  'options': '-m -x +s',
\  'down':    '40%'})

command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_
\ | diffthis | wincmd p | diffthis
